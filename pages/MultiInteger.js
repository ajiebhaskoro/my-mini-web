import Header from './layouts/Header'
import Footer from './layouts/Footer'
import InputMul from './component/InputMul'

import { Container, Row, Col } from 'react-bootstrap';

const MultiInteger = () => {
    return (
        <>
        <Header />
            <Container>
                <Row>
                    <Col></Col>
                    <Col><InputMul /></Col>
                    <Col></Col>              
                </Row>
            </Container>
        <Footer />
        </>
    )
}

export default MultiInteger;