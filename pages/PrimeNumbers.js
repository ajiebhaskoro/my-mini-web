import Header from './layouts/Header'
import Footer from './layouts/Footer'
import Prime from './component/PrimeNum'

import { Container, Row, Col } from 'react-bootstrap';

const PrimeNumber = () => {
    return (
        <>
        <Header />
            <Container>
                <Row>
                    <Col></Col>
                    <Col><Prime /></Col>
                    <Col></Col>        
                </Row>
            </Container>
        <Footer />
        </>
    )
}

export default PrimeNumber;