import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './store/reducer'


export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}