import Header from './layouts/Header'
import Footer from './layouts/Footer'
import Fibonacci from './component/FibboSolver'
import { Container, Row, Col } from 'react-bootstrap';

const FibonacciSolver = () => {
    return (
        <>
        <Header />
            <Container>
                <Row>
                    <Col></Col>
                    <Col><Fibonacci /></Col>
                    <Col></Col>
                    
                </Row>
            </Container>
        <Footer />
        </>
    )
}

export default FibonacciSolver;