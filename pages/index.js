// import styles from '../styles/Home.module.css'
import Header from './layouts/Header'
import Footer from './layouts/Footer'
import Link from 'next/link'
import { Container, Row, Col } from 'react-bootstrap';
import { Spring } from 'react-spring/renderprops.cjs'

export default function Home() {
  return (
    <>
      <Header /> 
        <Container classname="containerIndex">
          <Spring 
            from={{ opacity : 0 }} 
            to={{ opacity : 1 }}
            config={{ duration : 1000 }}>
              { props => 
                <div style = {props}> 
                  <Row className="justify-content-md-center">
                    <Col xs={12} sm={12} md={12}>
                      <h1 className="text-center">Welcome to My Mini Web</h1>  
                    </Col>
                  </Row>
                </div>}
            </Spring>

            <br/>


            <Spring 
            from={{ opacity : 0, marginTop : 100}} 
            to={{ opacity : 1, marginTop : 0 }}
            config={{ delay : 2000, duration : 1000 }}>
              { props => 
                <div style = {props}> 
                  <Row className="justify-content-md-center">
                    <Col xs={12} sm={6} md={12}>
                      <p className="text-center">There are four features in this website : <Link href="/SumInteger"><a>Sum of Integer</a></Link>, <Link href="/MultiInteger"><a>Multiplying Two Integer</a></Link>, <Link href="/PrimeNumbers"><a>Prime Number Solver</a></Link> and <Link href="/FibonacciSolver"><a>Fibonacci Solver</a></Link></p>  
                    </Col>
                  </Row>
                </div>}
            </Spring>

            <br/>

            <Spring 
            from={{ opacity : 0, marginRight : 500}} 
            to={{ opacity : 1, marginRight : 0 }}
            config={{ delay : 4000, duration : 1000 }}>
              { props => 
                <div style = {props}> 
                  <Row className="justify-content-md-center row">
                    <Col className="text-center" xs={12} sm={12} md={6}>
                      <img src="/barney_square.gif" style={{maxWidth : '40%', height : 'auto', borderRadius: '50%', marginBottom: '2rem'}}/>
                    </Col>
                  </Row>
                  <Row className="justify-content-md-center">
                    <Col xs={12} sm={6} md={6}>
                      <p className="text-center">Enjoy!</p> 
                    </Col>
                  </Row>
                </div>}
            </Spring>

        </Container>
      <Footer />
    </>
  )
}
