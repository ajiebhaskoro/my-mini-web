import { connect } from 'react-redux';
// import { useState } from 'react';
import { Modal, Button } from "react-bootstrap";


function ResultPrimeFibbo(props) {

  const { state } = props;

    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Result
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            The first {state.firstInt} numbers : 
          </p>  
          <h4>{(state.result)}</h4>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }

const mapStateToProps = state => ({
  state : state.primeFibo
})

export default connect(mapStateToProps,null)(ResultPrimeFibbo);