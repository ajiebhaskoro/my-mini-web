import { connect } from 'react-redux';
// import { useState } from 'react';
import { Modal, Button } from "react-bootstrap";


function Result(props) {

  const { state } = props;
    
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Result
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            The result of {state.firstInt} and {state.secondInt} is : 
          </p>  
          <h4>{state.result}</h4>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }

const mapStateToProps = state => ({
  state : state.main
})

export default connect(mapStateToProps,null)(Result);