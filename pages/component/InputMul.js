import { Card } from 'react-bootstrap';
import { connect } from 'react-redux';
import { useState } from 'react';
import * as action from '../redux/actions/mainActions';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import ResultModal from './Result';


function Input(props){

    const { setFirstInt, setSecondInt, mulInt } = props;

    const [ firstInt, setFirst ] = useState(0);
    const [ secondInt, setSecond ] = useState(0);

    const [addModalOpen, setAddModalOpen] = useState(false);

    const handleAddClick = () => {
      setAddModalOpen(true);
    };
    const handleClose = ()=>{
      setAddModalOpen(false)
    }

    return (
        <>
            <Container>
                <Card style={{ minWidth: '30rem' , minHeight: '15rem'}}>
                    <Card.Body>

                        <Form>
                            <Row className="justify-content-md-center">
                                <Col xs={12} sm={10} md={12}>
                                    <h2 className="text-center">Multiplification of Two Integer</h2>
                                </Col>
                            </Row>

                            <br/>

                            <Row>
                                <Col xs={12} sm={12} md={{span : 4, offset: 1}} className="text-center">
                                    <Form.Control type="number" className="text-center num-size" value={parseInt(firstInt).toString()} onChange={ e => setFirst(e.target.value)} />
                                    <Form.Text className="text-muted">
                                        Input Your First Number
                                    </Form.Text>
                                </Col> 
                                <Col xs={12} sm={12} md={2} className="text-center">
                                    <Form.Text style={{fontSize : 'X-large'}}>
                                        By
                                    </Form.Text>
                                </Col>
                                <Col xs={12} sm={12} md={4} className="text-center">
                                    <Form.Control type="number" className="text-center num-size" value={parseInt(secondInt).toString()} onChange={ e => setSecond(e.target.value)} />
                                    <Form.Text className="text-muted">
                                        Input Your Second Number
                                    </Form.Text>
                                </Col>
                            </Row>

                            <br/>

                            <Row className="justify-content-md-center">
                                <Col xs={12} sm={12} md={4} className="text-center">
                                    <Button className="btn btn-primary" onClick={() => {setFirstInt(firstInt), setSecondInt(secondInt), mulInt(), handleAddClick()}}>Submit</Button>
                                </Col>
                            </Row>
                        </Form>

                        <ResultModal show={addModalOpen} onHide={() => handleClose()}>

                        </ResultModal>

                    </Card.Body>
                </Card>
            </Container>
        </>
    )
}

const mapStateToProps = state => ({
    state : state.main
})

const mapDispatchToProps = {
    setFirstInt : action.setFirstInt,
    setSecondInt : action.setSecondInt,
    mulInt : action.mulInt
}

export default connect(mapStateToProps, mapDispatchToProps)(Input)