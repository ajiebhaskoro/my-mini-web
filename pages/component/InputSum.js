import { Card } from 'react-bootstrap';
import { connect } from 'react-redux';
import { useState } from 'react';
import * as action from '../redux/actions/mainActions';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import ResultModal from './Result';


function Input(props){

    const { setFirstInt, setSecondInt, sumInt } = props;

    const [ firstInt, setFirst ] = useState(0);
    const [ secondInt, setSecond ] = useState(0);

    const [addModalOpen, setAddModalOpen] = useState(false);

    const handleAddClick = () => {
      setAddModalOpen(true);
    };
    const handleClose = ()=>{
      setAddModalOpen(false)
    };
    const handleChangeFirstInt = (e) =>{
        if (e.target.value == '') {
            e.target.value = 0
          }
        
        setFirst(e.target.value)
    };
    const handleChangeSecondInt = (e) =>{
        if (e.target.value == '') {
            e.target.value = 0
          }
        
        setSecond(e.target.value)
    };

    return (
        <>
            <Container>
                <Card style={{ minWidth: '30rem' , minHeight: '15rem'}}>
                    <Card.Body>

                        <Form>
                            <Row className="justify-content-md-center">
                                <Col sm={12} md={12}>
                                    <h2 className="text-center">Sum of Integer</h2>
                                </Col>
                            </Row>
                            
                            <br/>

                            <Row>
                                <Col sm={12} md={{span : 4, offset: 1}} className="text-center">
                                    <Form.Control type="number" className="text-center num-size" value={parseInt(firstInt).toString()} onChange={ handleChangeFirstInt } />
                                    <Form.Text className="text-muted">
                                        Input Your First Number
                                    </Form.Text>
                                </Col> 
                                <Col sm={12} md={2} className="text-center">
                                    <Form.Text style={{fontSize : 'X-large'}}>
                                        +
                                    </Form.Text>
                                </Col>
                                <Col sm={12} md={4} className="text-center">
                                    <Form.Control type="number" className="text-center num-size" value={parseInt(secondInt).toString()} onChange={ handleChangeSecondInt } />
                                    <Form.Text className="text-muted">
                                        Input Your Second Number
                                    </Form.Text>
                                </Col>
                            </Row>

                            <br/>

                            <Row className="justify-content-md-center">
                                <Col sm={12} md={4} className="text-center">
                                    <Button className="btn btn-primary" onClick={() => {setFirstInt(firstInt), setSecondInt(secondInt), sumInt(), handleAddClick()}}>Submit</Button>
                                </Col>
                            </Row>
                        </Form>

                        <ResultModal show={addModalOpen} onHide={() => handleClose()}>

                        </ResultModal>

                    </Card.Body>
                </Card>
            </Container>
        </>
    )
}

const mapStateToProps = state => ({
    state : state.main
})

const mapDispatchToProps = {
    setFirstInt : action.setFirstInt,
    setSecondInt : action.setSecondInt,
    sumInt : action.sumInt
}

export default connect(mapStateToProps, mapDispatchToProps)(Input)