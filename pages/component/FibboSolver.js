import { Card } from 'react-bootstrap';
import { connect } from 'react-redux';
import { useState } from 'react';
import * as action from '../redux/actions/mainActions';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import ResultModal from './ResultPrimeFibbo';


function Input(props){

    const { setFirstInt, fiboSolver } = props;

    const [ firstInt, setFirst ] = useState(0);

    const [addModalOpen, setAddModalOpen] = useState(false);

    const handleAddClick = () => {
      setAddModalOpen(true);
    };
    const handleClose = ()=>{
      setAddModalOpen(false)
    }
    const handleChange = (e) =>{
        if (e.target.value == '') {
            e.target.value = 0
          }
        
        setFirst(e.target.value)
    }

    return (
        <>
            <Container>
            <Card style={{ minWidth: '30rem' , minHeight: '15rem'}}>
                    <Card.Body>

                        <Form>
                            <Row className="justify-content-md-center">
                                <Col sm={12} md={12}>
                                    <h2 className="text-center">Fibonacci Numbers</h2>
                                </Col>
                            </Row>

                            <br/>

                            <Row className="justify-content-md-center">
                                <Col sm={12} md={4} className="text-center">
                                    <Form.Control type="number" className="text-center num-size" value={parseInt(firstInt).toString()} onChange={handleChange}/>
                                    <Form.Text className="text-muted">
                                        Input your first N fibonacci number
                                    </Form.Text>
                                </Col> 
                            </Row>

                            <br/>

                            <Row className="justify-content-md-center">
                                <Col sm={12} md={4} className="text-center">
                                    <Button className="btn btn-primary" onClick={() => {setFirstInt(firstInt), fiboSolver(), handleAddClick()}}>Submit</Button>
                                </Col>
                            </Row>
                        </Form>

                        <ResultModal show={addModalOpen} onHide={() => handleClose()}>
                        
                        </ResultModal>

                    </Card.Body>
                </Card>
            </Container>
        </>
    )
}

const mapStateToProps = state => ({
    state : state.fibonacci
})

const mapDispatchToProps = {
    setFirstInt : action.setFirstInt,
    fiboSolver : action.fiboSolver
}

export default connect(mapStateToProps, mapDispatchToProps)(Input)