import Header from './layouts/Header'
import Footer from './layouts/Footer'
import InputSum from './component/InputSum'

import { Container, Row, Col } from 'react-bootstrap';

const SumInteger = () => {
    return (
        <>
        <Header />
            <Container>
                <Row>
                    <Col></Col>
                    <Col><InputSum /></Col>
                    <Col></Col>
                </Row>
            </Container>
        <Footer />
        </>
    )
}

export default SumInteger;