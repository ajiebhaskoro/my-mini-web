import * as reducer from './mainReducer';
import { combineReducers } from "redux";

const rootReducer = combineReducers({
    main : reducer.mainReducer,
    primeFibo : reducer.primeFiboReducer,
})

export default rootReducer;