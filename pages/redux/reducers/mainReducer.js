import * as t from '../type'

const initalState = {
    firstInt : 0,
    secondInt : 0,
    result : 0
}

const primeFiboState = {
    firstInt : 0,
    result : [],
}

function primeNumbersSolver(thres) {
    var primes = [], n = 2;

    function isPrime(n) {
        var sqrtn = Math.sqrt(n);

        for (var i = 2; i <= sqrtn; i++){
            if (n % i === 0) return false;
        }
        return true;
    }

    do if (isPrime(n++)) {
        primes.push(n - 1);
    }
    while (primes.length < thres);
    
    return primes;
}

function fibonacciSolver(n) {
	const list = [0, 1];
	for (let x = 2; x < n; x += 1) {
		list.push(list[x - 2] + list[x - 1]);
	}
	return list;
}

export const mainReducer = (state = initalState, action) => {
    switch (action.type){
        case t.SET_FIRST_INT :
            return Object.assign({}, state, {
                firstInt : parseInt(action.value),
            })
        case t.SET_SECOND_INT :
            return Object.assign({}, state, {
                secondInt : parseInt(action.value),
            })
        case t.SUM_INT :
            const sumResult = parseInt(state.firstInt) + parseInt(state.secondInt)
            return Object.assign({}, state, {
                result : sumResult
            })
        case t.MUL_INT :
            const mulResult = parseInt(state.firstInt) * parseInt(state.secondInt)
            return Object.assign({}, state, {
                result : mulResult
            })
        default :
            return {...state}
    }
}

export const primeFiboReducer = (state = primeFiboState, action) => {
    switch (action.type){
        case t.SET_FIRST_INT :
            return Object.assign({}, state, {
                firstInt : parseInt(action.value),
            })
        case t.PRIME_SOLVER : 
            let primeResult = 0
            
            if(parseInt(state.firstInt) <= 1){
                primeResult = 'NOT VALID'
            } else {
                primeResult = primeNumbersSolver(parseInt(state.firstInt)).join(", ")
            }

            return Object.assign({}, state, {
                result : primeResult
            })

        case t.FIBO_SOLVER : 
            let fiboResult = [0,1]

            if(parseInt(state.firstInt) == 1){
                fiboResult = fiboResult[0]
            } else if(parseInt(state.firstInt) <= 1){
                fiboResult = 'NOT VALID'
            } else {
                fiboResult = fibonacciSolver(parseInt(state.firstInt)).join(", ")
            }

            return Object.assign({}, state, {
            result : fiboResult
        })
        default :
            return {...state}
    }
}