import { put, take, call, all, delay } from 'redux-saga/effects';

import * as action from '../actions/mainActions'

function* sumSaga(){
    alert('This is Sum Integer Page');
    yield delay(2000);

}

function* multiSaga(){
    alert('This is Multiply Integer Page');
    yield delay(2000);

}

function* primeSaga(){
    alert('This is Prime Numbers Page');
    yield delay(2000);
}

function* fiboSaga(){
    alert('This is Fibonacci Solver Page');
    yield delay(2000);
}

function* homeSaga(){
    console.log('on page Home');    
}

// watcher saga

function* rootSaga(){
    if (window.location.pathname == "/SumInteger"){
        yield take(action.sumInt);
        yield call(sumSaga)
    } else if (window.location.pathname == "/MultiInteger"){
        yield take(action.mulInt);
        yield call(multiSaga)
    } else if (window.location.pathname == "/PrimeNumbers"){
        yield take(action.primeSolver);
        yield call(primeSaga)
    } else if (window.location.pathname == "/FibonacciSolver"){
        yield take(action.fiboSolver);
        yield call(fiboSaga)
    } else {
        yield call(homeSaga)
    }
}

export default rootSaga;