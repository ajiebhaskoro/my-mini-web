import * as t from '../type'

export const setFirstInt = (inputNumber) => ({
    type : t.SET_FIRST_INT,
    value: inputNumber
})

export const setSecondInt = (inputNumber) => ({
    type : t.SET_SECOND_INT,
    value: inputNumber
})

export const sumInt = () => ({
    type : t.SUM_INT
})

export const mulInt = () => ({
    type : t.MUL_INT
})

export const primeSolver = () => ({
    type : t.PRIME_SOLVER
})

export const fiboSolver = () => ({
    type : t.FIBO_SOLVER
})
