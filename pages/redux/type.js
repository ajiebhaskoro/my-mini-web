export const SET_FIRST_INT = "SET_FIRST_INT";
export const SET_SECOND_INT = "SET_SECOND_INT";
export const SUM_INT =  "SUM_INT";
export const MUL_INT = "MUL_INT"; 
export const PRIME_SOLVER = "PRIME_SOLVER"; 
export const FIBO_SOLVER = "FIBO_SOLVER"; 
