// import { createStore, applyMiddleware } from 'redux';
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import reducer from './reducers/rootReducer'
import logger from 'redux-logger';
// import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas/saga'

const devMode = process.env.NODE_ENV === 'development';
const saga = createSagaMiddleware();
const middleware = [...getDefaultMiddleware({ thunk: false }), saga];

if (devMode) {
  middleware.push(logger);
}

const initializeStore = () => {
  const store = configureStore({
    reducer,
    devTools: devMode,
    middleware,
  });

  saga.run(rootSaga);
  return store;
};

export default initializeStore;