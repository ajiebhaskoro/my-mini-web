import Head from 'next/head';
import { Navbar, Nav } from 'react-bootstrap';

const Header = () => (
    <div className="containerHeader">
        <Head>
            <title>Ajie Bhaskoro | Okadoc Test</title>
        </Head>
        <Navbar fixed="top" collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Navbar.Brand href="/">Ajie Bhaskoro</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="ml-auto"> 
                    <Nav.Link href="/SumInteger">Sum of Integers</Nav.Link>
                    <Nav.Link href="/MultiInteger">Multiplying Integers</Nav.Link>
                    <Nav.Link href="/PrimeNumbers">Prime Numbers</Nav.Link>
                    <Nav.Link href="/FibonacciSolver">Fibonacci Solver</Nav.Link>

                    {/* <NavDropdown title="List" id="collasible-nav-dropdown">
                        <NavDropdown.Item href="/SumInteger">Sum</NavDropdown.Item>
                        <NavDropdown.Item href="/MultiInteger">Multiply</NavDropdown.Item>
                        <NavDropdown.Item href="/PrimeNumbers">Prime Number</NavDropdown.Item>
                        <NavDropdown.Item href="/FibonacciSolver">Fibonacci Solver</NavDropdown.Item>
                    </NavDropdown> */}
                </Nav>
            </Navbar.Collapse>
        </Navbar>

    </div>

)

export default Header;