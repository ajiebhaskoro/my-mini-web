import { Navbar, Container,} from 'react-bootstrap';

function AppFooter() {
    return(
            <Navbar bg="dark" variant="dark" fixed="bottom" className="justify-content-md-center">
                <div style={{color : "silver"}}>
                    &copy; 2020 | Ajie Bhaskoro
                </div>
            </Navbar>
    )
}

export default AppFooter;